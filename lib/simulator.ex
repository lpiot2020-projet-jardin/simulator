defmodule Simulator do
  use GenServer
  require Logger

  @server_url "http://127.0.0.1:4000/api/spaces"

  # Public API
  def start_link(cycle_duration) do
    GenServer.start_link(__MODULE__, cycle_duration: cycle_duration)
  end

  @impl true
  def init(opts) do
    [cycle_duration: cycle_duration] = opts

    Process.send(self(), :up, [])

    Logger.info("Initiated simulator cycle with cycle duration of #{cycle_duration} seconds")

    {:ok, %{cycle_duration: cycle_duration, actual_cycle: cycle_duration}}
  end

  # Callbacks

  @impl true
  def handle_info(:up, %{cycle_duration: cycle_duration, actual_cycle: actual_cycle} = state) do
    Logger.debug("Handling correct cycle")

    if actual_cycle - 1 <= 0 do
      state = Map.put(state, :actual_cycle, cycle_duration)
      Process.send_after(self(), :down, 1000)

      Logger.debug("Correct cycles came to an end.")

      {:noreply, state}
    else
      state = Map.put(state, :actual_cycle, actual_cycle - 1)
      Process.send_after(self(), :up, 1000)

      message = prepare_message(:up)
      IO.inspect(message)

      {:noreply, state}
    end
  end

  @impl true
  def handle_info(:down, %{cycle_duration: cycle_duration, actual_cycle: actual_cycle} = state) do
    Logger.debug("Handling incorrect cycle")

    if actual_cycle - 1 <= 0 do
      state = Map.put(state, :actual_cycle, cycle_duration)
      Process.send_after(self(), :up, 1000)

      Logger.debug("Incorrect cycles came to an end.")

      {:noreply, state}
    else
      state = Map.put(state, :actual_cycle, actual_cycle - 1)
      Process.send_after(self(), :down, 1000)

      message = prepare_message(:down)
      IO.inspect(message)

      #Req.post!(@server_url, {:json, message})

      {:noreply, state}
    end
  end

  defp prepare_message(:up) do
    %{
      "my_json_message" => "when it's nice"
    }
  end

  defp prepare_message(:down) do
    %{
      "my_json_message" => "when it's bad"
    }
  end
end
